module RegisterMemory
    (
        input  [4:0] A1, A2, A3,
        input  WE3,
        input  [31:0] WD3,
        input  clk,
		output reg [31:0] RD1, RD2
    );

    reg [31:0] rf[31:0];

    integer i;
    initial begin
        for(i=0;i<32;i++)
            rf[i] = 32'h 0;
    end


    always@ (*) begin
            RD1 = rf[A1];
            RD2 = rf[A2];
    end
    //assign rf[0] = 0;

    always @ (posedge clk)
		if (WE3 && A3 != 5'h 0)
			rf[A3] = WD3;

endmodule
