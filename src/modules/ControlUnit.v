module MainDecoder
    (
        input [5:0] Opcode,
        output reg RegWrite, RegDst, ALUSrc, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr,
        output reg [1:0] ALUOp
    );

    always@(*) begin
        case (Opcode)
            6'b000000: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1101000000;
			6'b100011: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1010000100;
			6'b101011: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b0X10001X00;
			6'b000100: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b0X00110X00;
			6'b001000: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1010000000;
			6'b000011: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1XXXXX0X10;
			6'b000111: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b0XXXXX0X01;
			6'b011111: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1101100000;
        endcase
    end

endmodule

module ALUOpDecoder
    (
        input [1:0] ALUOp,
        input [5:0] Funct,
        input [4:0] Shamt,
        output reg [3:0] ALUControl
    );

    always@(*) begin
        case (ALUOp)
            'b 00:
                ALUControl = 'b 0010;
            'b 01:
                ALUControl = 'b 0110;
            'b 10:
                case (Funct)
                    'b 100000: ALUControl = 'b 0010;
                    'b 100010: ALUControl = 'b 0110;
                    'b 100100: ALUControl = 'b 0000;
                    'b 100101: ALUControl = 'b 0001;
                    'b 101010: ALUControl = 'b 0111;
                    default: ALUControl = 'b 1111;
                endcase
            'b 11:
                case ({Funct, Shamt})
                    'b 01000000000 : ALUControl = 'b 1000;
                    'b 01000000100 : ALUControl = 'b 1001;
                    default: ALUControl = 'b 1111;
                endcase
            default:
                ALUControl = 'b 1111;
        endcase
    end
endmodule

module ControlUnit
    (
        input [5:0] Opcode,
        input [5:0] Funct,
        input [4:0] Shamt,
        output RegWrite, RegDst, ALUSrc, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr,
        output [3:0] ALUControl
    );
    wire [1:0] ALUOp;
    MainDecoder  MainDecoder_0 (Opcode, RegWrite, RegDst, ALUSrc, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr, ALUOp);
    ALUOpDecoder ALUOpDecoder_0(ALUOp, Funct, Shamt, ALUControl);

endmodule
