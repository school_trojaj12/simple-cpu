lw 	$a1, 0x00000008($0) # size_of_array
lw 	$a0, 0x0000000C($0) # address_of_array

addi 	$t0,$0,1
#	slt	$v0, $at, $a1 #test input
#	beq	$0, $v0, konec
sub	$a1, $a1, $t0

add	$a1, $a1, $a1
add	$a1, $a1, $a1

add	$a1, $a1, $a0


addi	$a2, $a0, 0	#i

sort:
	beq	$a2, $a1 konec
	addi	$a3, $a2, 4 #i+1

	lw	$t0, 0($a2)
	lw	$t1, 0($a3)
	slt	$t2, $t1, $t0
	beq	$t2, $0, next
		#else swap
		sw	$t0 0($a3)
		sw	$t1 0($a2)
		addi	$a2, $a0, 0
		beq	$0, $0, sort

next:
	addi	$a2, $a2, 4
	beq	$0, $0, sort

konec:
	beq $0, $0 konec
