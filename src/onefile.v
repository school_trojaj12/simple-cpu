module processor( input         clk, reset,
                  output [31:0] PC,
                  input  [31:0] instruction,
                  output        WE,
                  output [31:0] address_to_mem,
                  output [31:0] data_to_mem,
                  input  [31:0] data_from_mem
                );

        wire        RegWrite, RegDst, ALUSrc, Branch, MemToReg, PCSrcJal, PCSrcJr;
        wire        zero;
        wire [3:0]  ALUControl;
        wire [4:0] MX_1__MX_0, MUX4__RM_A3;
        wire [31:0] RM_rd1__ALU_a, RM_rd2__MX_2, MX_3__MX5_0, MUX5__RM_WD3, ALU_res__DM_a, MX_2__ALU_2, EXP_out__MX_2, PC_after_4_added, PC_after_BR_adder, PC_nove;
        reg [31:0] PC_tmp;

    ControlUnit     CU  ( instruction[31:26], instruction[5:0], instruction[10:6], RegWrite,
                            RegDst, ALUSrc, Branch, WE, MemToReg, PCSrcJal, PCSrcJr, ALUControl );

    RegisterMemory  RM  ( instruction[25:21], instruction[20:16], MUX4__RM_A3, RegWrite, MUX5__RM_WD3 ,
                            clk, RM_rd1__ALU_a, RM_rd2__MX_2);
                        assign data_to_mem = RM_rd2__MX_2;


    ALU             ALU ( RM_rd1__ALU_a, MX_2__ALU_2, ALUControl, zero, address_to_mem );

    Expand_16_32        EXP ( instruction[15:0], EXP_out__MX_2 );

    Multiplexor #(5)    MX_1( instruction[20:16], instruction[15:11], RegDst ,MX_1__MX_0);

    Multiplexor         MX_2( RM_rd2__MX_2,EXP_out__MX_2 , ALUSrc , MX_2__ALU_2 );

    Multiplexor         MX_3( data_from_mem, address_to_mem, ~MemToReg, MX_3__MX5_0);


    Multiplexor #(5)    MX_4( MX_1__MX_0, 5'd 31, PCSrcJal, MUX4__RM_A3);
    Multiplexor         MX_5( MX_3__MX5_0, PC_after_4_added, PCSrcJal, MUX5__RM_WD3);


    Adder               ADPC(PC_tmp, 'h 4, PC_after_4_added);
    Adder               ADBR(PC_after_4_added, (EXP_out__MX_2 << 2), PC_after_BR_adder);


    LastMux             LM  ( RM_rd1__ALU_a, { PC_after_4_added[31:28],  instruction[25:0], 2'b00 } ,PC_after_BR_adder,PC_after_4_added, PCSrcJr, PCSrcJal, zero && Branch, PC_nove );
    //Multiplexor         MX6( PC_after_4_added, PC_after_BR_adder ,zero && Branch ,PC_nove );
    always@( posedge clk )
    begin
		if( reset == 1 )
          	PC_tmp = 32'h 00;
      	else
      		PC_tmp = PC_nove;
    end

    assign PC = PC_tmp;

endmodule

//-----------------------------------------------------------------------------------------------------

module LastMux
    (
        input [31:0] d0,d1,d2,d3,
        input s0, s1, s2,
        output reg [31:0] ven
    );

    always@ (*) begin
        if( s0 )
            ven = d0;
        else if( s1 )
            ven = d1;
        else if( s2 )
            ven = d2;
        else
            ven = d3;
    end

endmodule
//-----------------------------------------------------------------------------------------------------
module Adder
    (
        input  [31:0] A, B,
        output [31:0] Res
    );
    assign Res = A + B;
endmodule
//-----------------------------------------------------------------------------------------------------
module ALU
    (
        input  [31:0] ScrA, ScrB,
        input  [3:0] ALUControl,
        output reg Zero,
        output reg [31:0] ALUResult
    );

    always@ ( * ) begin
        case ( ALUControl )
            4'b 0010: ALUResult = ScrA + ScrB;
            4'b 0110: ALUResult = ScrA - ScrB;
            4'b 0000: ALUResult = ScrA & ScrB;
            4'b 0001: ALUResult = ScrA | ScrB;
            4'b 0011: ALUResult = ScrA ^ ScrB;
            4'b 0111: ALUResult = $signed(ScrA) < $signed(ScrB);
            4'b 1000://4 unsigned add
                begin
                    ALUResult[7:0]   = ScrA[7:0]   + ScrB[7:0];
					ALUResult[15:8]  = ScrA[15:8]  + ScrB[15:8];
					ALUResult[23:16] = ScrA[23:16] + ScrB[23:16];
					ALUResult[31:24] = ScrA[31:24] + ScrB[31:24];
                end
            4'b 1001://4 unsigned satur add
                begin
                    ALUResult[7:0] 	 = ( ScrA[7:0] + ScrB[7:0] ) < ScrA[7:0]
                                            ? 255
                                            : ( ScrA[7:0] + ScrB[7:0] );

					ALUResult[15:8]  = ( ScrA[15:8] + ScrB[15:8] ) < ScrA[15:8]
                                            ? 255
                                            : ( ScrA[15:8] + ScrB[15:8] );

					ALUResult[23:16] = ( ScrA[23:16] + ScrB[23:16] ) < ScrA[23:16]
                                            ? 255
                                            : ( ScrA[23:16] + ScrB[23:16] );

					ALUResult[31:24] = ( ScrA[31:24] + ScrB[31:24] ) < ScrA[31:24]
                                            ? 255
                                            : ( ScrA[31:24] + ScrB[31:24] );
                end
            default:
                ALUResult = ScrA + ScrB;
        endcase
    end

    always@( ALUResult )
        Zero = ( ALUResult == 0 ) ? 1 : 0;

endmodule
//-----------------------------------------------------------------------------------------------------
module MainDecoder
    (
        input [5:0] Opcode,
        output reg RegWrite, RegDst, ALUSrc, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr,
        output reg [1:0] ALUOp
    );

    always@(*) begin
        case (Opcode)
            6'b000000: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1101000000;
			6'b100011: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1010000100;
			6'b101011: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b0X10001X00;
			6'b000100: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b0X00110X00;
			6'b001000: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1010000000;
			6'b000011: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1XXXXX0X10;
			6'b000111: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b0XXXXX0X01;
			6'b011111: {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr } = 10'b1101100000;
        endcase
    end

endmodule
//-----------------------------------------------------------------------------------------------------
module ALUOpDecoder
    (
        input [1:0] ALUOp,
        input [5:0] Funct,
        input [4:0] Shamt,
        output reg [3:0] ALUControl
    );

    always@(*) begin
        case (ALUOp)
            'b 00:
                ALUControl = 'b 0010;
            'b 01:
                ALUControl = 'b 0110;
            'b 10:
                case (Funct)
                    'b 100000: ALUControl = 'b 0010;
                    'b 100010: ALUControl = 'b 0110;
                    'b 100100: ALUControl = 'b 0000;
                    'b 100101: ALUControl = 'b 0001;
                    'b 101010: ALUControl = 'b 0111;
                    default: ALUControl = 'b 1111;
                endcase
            'b 11:
                case ({Funct, Shamt})
                    'b 01000000000 : ALUControl = 'b 1000;
                    'b 01000000100 : ALUControl = 'b 1001;
                    default: ALUControl = 'b 1111;
                endcase
            default:
                ALUControl = 'b 1111;
        endcase
    end
endmodule
//-----------------------------------------------------------------------------------------------------
module ControlUnit
    (
        input [5:0] Opcode,
        input [5:0] Funct,
        input [4:0] Shamt,
        output RegWrite, RegDst, ALUSrc, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr,
        output [3:0] ALUControl
    );
    wire [1:0] ALUOp;
    MainDecoder  MainDecoder_0 (Opcode, RegWrite, RegDst, ALUSrc, Branch, MemWrite, MemToReg, PCSrcJal, PCSrcJr, ALUOp);
    ALUOpDecoder ALUOpDecoder_0(ALUOp, Funct, Shamt, ALUControl);

endmodule
//-----------------------------------------------------------------------------------------------------
module Expand_16_32
    (
        input  [15:0] in,
        output [31:0] out
    );

    assign out = { {16{in[15]}} , in };

endmodule
//-----------------------------------------------------------------------------------------------------
module Multiplexor # ( parameter width = 32)
    (
        input [width-1:0] d0,d1,
        input select,
        output [width-1:0] out
    );

    assign out = ~select ? d0 : d1;

endmodule
//-----------------------------------------------------------------------------------------------------
module RegisterMemory
    (
        input  [4:0] A1, A2, A3,
        input  WE3,
        input  [31:0] WD3,
        input  clk,
		output reg [31:0] RD1, RD2
    );

    reg [31:0] rf[31:0];

    integer i;
    initial begin
        for(i=0;i<32;i++)
            rf[i] = 32'h 0;
    end


    always@ (*) begin
            RD1 = rf[A1];
            RD2 = rf[A2];
    end
    //assign rf[0] = 0;

    always @ (posedge clk)
		if (WE3 && A3 != 5'h 0)
			rf[A3] = WD3;

endmodule